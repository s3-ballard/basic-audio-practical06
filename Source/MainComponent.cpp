/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"


//==============================================================================
MainComponent::MainComponent()
{
    setSize (500, 400);
    
    audioDeviceManager.initialiseWithDefaultDevices(2, 2);
    audioDeviceManager.addAudioCallback(this);
    audioDeviceManager.setMidiInputEnabled("MicroBrute", true);
    audioDeviceManager.addMidiInputCallback(String::empty, this);
    
    addAndMakeVisible(ampSlider);
    ampSlider.setSliderStyle(Slider::LinearHorizontal);
    ampSlider.setRange(0, 1);
    ampSlider.setBounds(10, 90, getWidth()-20, 40);
    ampSlider.addListener(this);
  
}

MainComponent::~MainComponent()
{
    audioDeviceManager.removeAudioCallback(this);
    audioDeviceManager.removeMidiInputCallback(String::empty, this);
}

void MainComponent::resized()
{

}

void MainComponent::audioDeviceIOCallback (const float** inputChannelData,
                            int numInputChannels,
                            float** outputChannelData,
                            int numOutputChannels,
                            int numSamples)
{
   // DBG (" audio device IO ");
    const float *inL = inputChannelData[0];
    const float *inR = inputChannelData[1];
    float *outL = outputChannelData[0];
    float *outR = outputChannelData[1];

    
    while (numSamples--)
    {
        *outL = (*inL * amp2);
        *outR = (*inR * amp2);
        
ampSlider.getValueObject().setValue (amp2);
        
        inL++;
        inR++;
        outL++;
        outR++;
    }
}

void MainComponent::audioDeviceAboutToStart(AudioIODevice *device)
{
    DBG (" audio device about to start");
}

void MainComponent::audioDeviceStopped()
{
    DBG (" audio device stopped");
}

void MainComponent::handleIncomingMidiMessage(MidiInput*, const MidiMessage& message)
{
     DBG ("message Recieved");
      if(message.isController())
        {
            if (message.getControllerNumber() == 1)
            {
                amp2 = (message.getControllerValue()) / 127.0;
                
            }
        }

}


void MainComponent::sliderValueChanged(Slider* slider)
{

 
}

